use std::env;

fn main() {
    let task = env::args().skip(1).next();

    match task.as_deref() {
        Some("1") => task1(),
        Some("2") => task2(),
        _ => {
            task1();
            task2();
        }
    }
}

fn task1() {
    let output = "TODO";

    println!("Task 1: {output}")
}

fn task2() {
    let output = "TODO";

    println!("Task 2: {output}")
}
