use std::{env, error::Error, str::FromStr};

fn main() {
    let task = env::args().skip(1).next();

    match task.as_deref() {
        Some("1") => task1(),
        Some("2") => task2(),
        _ => {
            task1();
            task2();
        }
    }
}

fn task1() {
    let output = common::split_input("\n")
        .into_iter()
        .map(|line| line.parse::<ElfPair>().unwrap())
        .filter(|pair| pair.fully_contains())
        .count();

    println!("Task 1: {output}")
}

fn task2() {
    let output = common::split_input("\n")
        .into_iter()
        .map(|line| line.parse::<ElfPair>().unwrap())
        .filter(|pair| pair.overlaps())
        .count();

    println!("Task 2: {output}")
}

struct ElfPair {
    first: (u16, u16),
    second: (u16, u16),
}

impl ElfPair {
    fn fully_contains(&self) -> bool {
        if self.first.0 >= self.second.0 && self.first.1 <= self.second.1 {
            return true;
        }

        self.first.0 <= self.second.0 && self.first.1 >= self.second.1
    }

    fn overlaps(&self) -> bool {
        self.first.1 >= self.second.0 && self.second.1 >= self.first.0
    }
}

impl FromStr for ElfPair {
    type Err = Box<dyn Error>;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (first, second) = s.split_once(",").ok_or("No comma found")?;
        let (fl, fr) = first.split_once("-").ok_or("No first dash found")?;
        let (sl, sr) = second.split_once("-").ok_or("No second dash found")?;

        Ok(Self {
            first: (fl.parse()?, fr.parse()?),
            second: (sl.parse()?, sr.parse()?),
        })
    }
}
