use std::fs;

pub fn split_example(split: &str) -> Vec<String> {
    fs::read_to_string("example")
        .unwrap()
        .split(split)
        .map(|s| s.to_owned())
        .collect()
}

pub fn read_input() -> String {
    fs::read_to_string("input").unwrap()
}

pub fn split_input(split: &str) -> Vec<String> {
    fs::read_to_string("input")
        .unwrap()
        .split(split)
        .map(|s| s.to_owned())
        .collect()
}
