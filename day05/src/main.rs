use std::{env, str::FromStr, error::Error};

fn main() {
    let task = env::args().skip(1).next();

    match task.as_deref() {
        Some("1") => task1(),
        Some("2") => task2(),
        _ => {
            task1();
            task2();
        }
    }
}

fn task1() {
    let stacks = common::split_input("\n").into_iter()
        .map(|line| line.parse::<Action>().unwrap())
        .fold(Stacks::default(), |mut stacks, action| {
            let elements = stacks.pop1(&action);
            stacks.push(&action, elements);
            stacks
        });

    println!("Task 1: {}", stacks.top_of_each_stack())
}

fn task2() {
    let stacks = common::split_input("\n").into_iter()
        .map(|line| line.parse::<Action>().unwrap())
        .fold(Stacks::default(), |mut stacks, action| {
            let elements = stacks.pop2(&action);
            stacks.push(&action, elements);
            stacks
        });

    println!("Task 2: {}", stacks.top_of_each_stack())
}

struct Stacks {
     stacks: [Vec<char>; 9],
}

impl Stacks {
    fn pop1(&mut self, action: &Action) -> Vec<char> {
        let source = &mut self.stacks[action.from - 1];
        (0..action.count).map(|_| source.pop().unwrap()).collect()
    }

    fn push(&mut self, action: &Action, mut elements: Vec<char>) {
        let dest = &mut self.stacks[action.to - 1];
        dest.append(&mut elements);
    }

    fn pop2(&mut self, action: &Action) -> Vec<char> {
        let source = &mut self.stacks[action.from - 1];
        source.drain((source.len()-action.count)..)
            .collect()
    }

    fn top_of_each_stack(&self) -> String {
        self.stacks.iter()
            .map(|stack| stack.last().unwrap().clone())
            .collect()
    }
}

impl Default for Stacks {
    fn default() -> Self {
        Self { stacks: [
            vec!['F', 'C', 'P', 'G', 'Q', 'R'],
            vec!['W', 'T', 'C', 'P'],
            vec!['B', 'H', 'P', 'M', 'C'],
            vec!['L', 'T', 'Q', 'S', 'M', 'P', 'R'],
            vec!['P', 'H', 'J', 'Z', 'V', 'G', 'N'],
            vec!['D', 'P', 'J'],
            vec!['L', 'G', 'P', 'Z', 'F', 'J', 'T', 'R'],
            vec!['N', 'L', 'H', 'C', 'F', 'P', 'T', 'J'],
            vec!['G', 'V', 'Z', 'Q', 'H', 'T', 'C', 'W'],
        ] }
    }
}

struct Action {
    count: usize,
    from: usize,
    to: usize,
}

impl FromStr for Action {
    type Err = Box<dyn Error>;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut parts = s.split_whitespace();
        let _ = parts.next().ok_or("Error skipping move: {s}")?;
        let count = parts.next().ok_or("No move found: {s}")?.parse()?;
        let _ = parts.next().ok_or("Error skipping from: {s}")?;
        let from = parts.next().ok_or("No move found: {s}")?.parse()?;
        let _ = parts.next().ok_or("Error skippintog from: {s}")?;
        let to = parts.next().ok_or("No move found: {s}")?.parse()?;

        Ok(Self { count, from, to })
    }
} 
