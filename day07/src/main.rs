use std::{env, error::Error, str::FromStr};

fn main() {
    let task = env::args().skip(1).next();

    match task.as_deref() {
        Some("1") => task1(),
        Some("2") => task2(),
        _ => {
            task1();
            task2();
        }
    }
}

fn task1() {
    let log: Vec<LogLine> = common::split_input("\n")
        .into_iter()
        .map(|line| line.parse::<LogLine>().unwrap())
        .collect();
    let fs: FileSystem = log.into();
    let output = fs.root.size_task1();

    println!("Task 1: {output}")
}

fn task2() {
    let log: Vec<LogLine> = common::split_input("\n")
        .into_iter()
        .map(|line| line.parse::<LogLine>().unwrap())
        .collect();
    let fs: FileSystem = log.into();

    let total_disk_space = 70_000_000;
    let total_used_disk_space = fs.root.size();
    let free_space = total_disk_space - total_used_disk_space;
    let needed_space = 30_000_000 - free_space;
    let sizes = fs.root.dir_sizes_task2();

    let output = sizes
        .into_iter()
        .filter(|&size| size >= needed_space)
        .min()
        .unwrap();

    println!("Task 2: {output}")
}

#[derive(Debug)]
struct FileSystem {
    root: Dir,
}

#[derive(Debug)]
enum Entry {
    Dir(Dir),
    File(File),
}

#[derive(Debug)]
struct Dir {
    name: String,
    entries: Vec<Entry>,
}

#[derive(Debug, PartialEq, Eq)]
struct File {
    name: String,
    size: u32,
}

impl FileSystem {
    fn add(&mut self, new_entry: Entry, parent_path: &Vec<String>) {
        let mut path_iter = parent_path.iter();
        path_iter.next().unwrap(); // ignore /
        self.root.add(new_entry, path_iter);
    }
}

impl Dir {
    fn add<'s, 'i>(
        &'s mut self,
        new_entry: Entry,
        mut path_iter: impl Iterator<Item = &'i String>,
    ) {
        match path_iter.next() {
            None => {
                self.entries.push(new_entry);
            }
            Some(child) => {
                let child = self
                    .entries
                    .iter_mut()
                    .filter_map(|e| match e {
                        Entry::Dir(dir) => Some(dir),
                        _ => None,
                    })
                    .find(|dir| &dir.name == child)
                    .expect("Could not find child");

                child.add(new_entry, path_iter);
            }
        }
    }

    fn size_task1(&self) -> u32 {
        let sum = self
            .entries
            .iter()
            .map(|e| match e {
                Entry::Dir(dir) => dir.size(),
                Entry::File(file) => file.size,
            })
            .sum();

        let sum = if sum > 100000 { 0 } else { sum };

        sum + self
            .entries
            .iter()
            .map(|e| match e {
                Entry::Dir(dir) => dir.size_task1(),
                Entry::File(_) => 0,
            })
            .sum::<u32>()
    }

    fn dir_sizes_task2(&self) -> Vec<u32> {
        let mut sizes: Vec<u32> = self
            .entries
            .iter()
            .filter_map(|e| match e {
                Entry::Dir(dir) => Some(dir.dir_sizes_task2()),
                Entry::File(_) => None,
            })
            .flatten()
            .collect();
        sizes.push(self.size());
        sizes
    }

    fn size(&self) -> u32 {
        self.entries
            .iter()
            .map(|e| match e {
                Entry::Dir(dir) => dir.size(),
                Entry::File(file) => file.size,
            })
            .sum()
    }
}

impl Default for FileSystem {
    fn default() -> Self {
        Self {
            root: Dir {
                name: "/".to_owned(),
                entries: vec![],
            },
        }
    }
}

impl From<Vec<LogLine>> for FileSystem {
    fn from(log: Vec<LogLine>) -> Self {
        log.into_iter()
            .fold(
                (FileSystem::default(), vec!["/".to_owned()]),
                |(mut fs, mut paths), next| {
                    match next {
                        LogLine::Cmd(Cmd::Cd(dir)) => {
                            match dir.as_str() {
                                "/" => (fs, paths), // ignore since part of FileSystem::default()
                                ".." => {
                                    paths.pop().unwrap();
                                    (fs, paths)
                                }
                                _ => {
                                    paths.push(dir);
                                    (fs, paths)
                                }
                            }
                        }
                        LogLine::Cmd(Cmd::Ls) => (fs, paths),
                        LogLine::LsOutput(LsOutputLine::Dir(LsDirOutputLine { dir_name })) => {
                            fs.add(
                                Entry::Dir(Dir {
                                    name: dir_name,
                                    entries: vec![],
                                }),
                                &paths,
                            );
                            (fs, paths)
                        }
                        LogLine::LsOutput(LsOutputLine::File(LsFileOutputLine {
                            file_size,
                            file_name,
                        })) => {
                            fs.add(
                                Entry::File(File {
                                    name: file_name,
                                    size: file_size,
                                }),
                                &paths,
                            );
                            (fs, paths)
                        }
                    }
                },
            )
            .0
    }
}

#[derive(Debug)]
enum LogLine {
    Cmd(Cmd),
    LsOutput(LsOutputLine),
}

#[derive(Debug)]
enum Cmd {
    Cd(String),
    Ls,
}

#[derive(Debug)]
enum LsOutputLine {
    Dir(LsDirOutputLine),
    File(LsFileOutputLine),
}

#[derive(Debug)]
struct LsDirOutputLine {
    dir_name: String,
}

#[derive(Debug)]
struct LsFileOutputLine {
    file_size: u32,
    file_name: String,
}

impl FromStr for LogLine {
    type Err = Box<dyn Error>;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut parts = s.split_whitespace();
        let start = parts.next().ok_or("Could not split: {s}")?;
        match start {
            "$" => {
                let cmd = parts.next().ok_or("Could not find cmd: {s}")?;
                match cmd {
                    "cd" => {
                        let dest = parts.next().ok_or("Could not find cd dest: {s}")?;
                        Ok(LogLine::Cmd(Cmd::Cd(dest.to_owned())))
                    }
                    "ls" => Ok(LogLine::Cmd(Cmd::Ls)),
                    _ => Err("Unknown cmd: {s}".into()),
                }
            }
            "dir" => {
                let dir_name = parts.next().ok_or("Missing file name: {s}")?.to_owned();

                Ok(LogLine::LsOutput(LsOutputLine::Dir(LsDirOutputLine {
                    dir_name,
                })))
            }
            file_size => {
                let file_size = file_size.parse()?;
                let file_name = parts.next().ok_or("Missing file name: {s}")?.to_owned();

                Ok(LogLine::LsOutput(LsOutputLine::File(LsFileOutputLine {
                    file_size,
                    file_name,
                })))
            }
        }
    }
}
