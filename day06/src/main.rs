use std::{env, collections::HashSet};

fn main() {
    let task = env::args().skip(1).next();

    match task.as_deref() {
        Some("1") => task1(),
        Some("2") => task2(),
        _ => {
            task1();
            task2();
        }
    }
}

fn task1() {
    let input = common::read_input();
    let output = input.chars().enumerate()
        .fold((vec![], None), |(mut acc, res), (i, c)| {
            if let Some(_) = res {
                return (acc, res);
            }
            if acc.len() >= 4 {
                acc.remove(0);
            }
            acc.push(c);
            let mut set: HashSet<char> = HashSet::with_capacity(4);
            acc.iter().for_each(|c| { set.insert(*c);} );
            if set.len() >= 4 {
                (acc, Some((i + 1, set)))
            } else {
                (acc, None)
            }
        })
        .1
        .unwrap()
        .0;

    println!("Task 1: {output}")
}

fn task2() {
    let input = common::read_input();
    let output = input.chars().enumerate()
        .fold((vec![], None), |(mut acc, res), (i, c)| {
            if let Some(_) = res {
                return (acc, res);
            }
            if acc.len() >= 14 {
                acc.remove(0);
            }
            acc.push(c);
            let mut set: HashSet<char> = HashSet::with_capacity(4);
            acc.iter().for_each(|c| { set.insert(*c);} );
            if set.len() >= 14 {
                (acc, Some((i + 1, set)))
            } else {
                (acc, None)
            }
        })
        .1
        .unwrap()
        .0;

    println!("Task 2: {output}")
}
