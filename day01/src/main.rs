fn main() {
    task1();
    task2();
}

fn task1() {
    let max_cal: u32 = common::split_input("\n\n")
        .into_iter()
        .map(|elf| {
            elf.split_whitespace()
                .map(|line| line.parse::<u32>().unwrap())
                .sum()
        })
        .max()
        .unwrap();

    println!("Task 1: {max_cal}")
}

fn task2() {
    let mut elfs: Vec<u32> = common::split_input("\n\n")
        .into_iter()
        .map(|elf| {
            elf.split_whitespace()
                .map(|line| line.parse::<u32>().unwrap())
                .sum()
        })
        .collect();

    elfs.sort_by(|a, b| b.cmp(a));
    let top_3 = elfs[0] + elfs[1] + elfs[2];

    println!("Task 2: {top_3}")
}
