use std::str::FromStr;

fn main() {
    task1();
    task2();
}

fn task1() {
    let total_score: u32 = common::split_input("\n")
        .into_iter()
        .map(|s| s.parse::<Battle1>().unwrap())
        .map(|b| b.score())
        .sum();

    println!("Task 1: {total_score}")
}

fn task2() {
    let total_score: u32 = common::split_input("\n")
        .into_iter()
        .map(|s| s.parse::<Battle2>().unwrap())
        .map(|b| b.score())
        .sum();

    println!("Task 2: {total_score}")
}

#[derive(Clone, Copy)]
enum RockPaperScissors {
    Rock,
    Paper,
    Scissors,
}

impl FromStr for RockPaperScissors {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "A" | "X" => Ok(RockPaperScissors::Rock),
            "B" | "Y" => Ok(RockPaperScissors::Paper),
            "C" | "Z" => Ok(RockPaperScissors::Scissors),
            s => Err(s.to_owned()),
        }
    }
}

struct Battle1 {
    opponent: RockPaperScissors,
    me: RockPaperScissors,
}

impl Battle1 {
    fn score(&self) -> u32 {
        let score = match self.me {
            RockPaperScissors::Rock => 1,
            RockPaperScissors::Paper => 2,
            RockPaperScissors::Scissors => 3,
        };

        score
            + match (self.opponent, self.me) {
                (RockPaperScissors::Rock, RockPaperScissors::Rock) => 3,
                (RockPaperScissors::Rock, RockPaperScissors::Paper) => 6,
                (RockPaperScissors::Rock, RockPaperScissors::Scissors) => 0,
                (RockPaperScissors::Paper, RockPaperScissors::Rock) => 0,
                (RockPaperScissors::Paper, RockPaperScissors::Paper) => 3,
                (RockPaperScissors::Paper, RockPaperScissors::Scissors) => 6,
                (RockPaperScissors::Scissors, RockPaperScissors::Rock) => 6,
                (RockPaperScissors::Scissors, RockPaperScissors::Paper) => 0,
                (RockPaperScissors::Scissors, RockPaperScissors::Scissors) => 3,
            }
    }
}

impl FromStr for Battle1 {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut split = s.split(" ");
        let opponent = split
            .next()
            .ok_or(format!("No opponent found : {s}"))?
            .parse()?;
        let me = split.next().ok_or(format!("No me found : {s}"))?.parse()?;

        Ok(Self { opponent, me })
    }
}

struct Battle2 {
    opponent: RockPaperScissors,
    outcome: Outcome,
}

impl Battle2 {
    fn score(&self) -> u32 {
        let score = match &self.outcome {
            Outcome::Lose => 0,
            Outcome::Draw => 3,
            Outcome::Victory => 6,
        };

        let me = match (self.opponent, &self.outcome) {
            (RockPaperScissors::Rock, Outcome::Lose) => RockPaperScissors::Scissors,
            (RockPaperScissors::Rock, Outcome::Draw) => RockPaperScissors::Rock,
            (RockPaperScissors::Rock, Outcome::Victory) => RockPaperScissors::Paper,
            (RockPaperScissors::Paper, Outcome::Lose) => RockPaperScissors::Rock,
            (RockPaperScissors::Paper, Outcome::Draw) => RockPaperScissors::Paper,
            (RockPaperScissors::Paper, Outcome::Victory) => RockPaperScissors::Scissors,
            (RockPaperScissors::Scissors, Outcome::Lose) => RockPaperScissors::Paper,
            (RockPaperScissors::Scissors, Outcome::Draw) => RockPaperScissors::Scissors,
            (RockPaperScissors::Scissors, Outcome::Victory) => RockPaperScissors::Rock,
        };

        score
            + match me {
                RockPaperScissors::Rock => 1,
                RockPaperScissors::Paper => 2,
                RockPaperScissors::Scissors => 3,
            }
    }
}

impl FromStr for Battle2 {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut split = s.split(" ");
        let opponent = split
            .next()
            .ok_or(format!("No opponent found : {s}"))?
            .parse()?;
        let outcome = split
            .next()
            .ok_or(format!("No outcome found : {s}"))?
            .parse()?;

        Ok(Self { opponent, outcome })
    }
}

enum Outcome {
    Lose,
    Draw,
    Victory,
}

impl FromStr for Outcome {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "X" => Ok(Outcome::Lose),
            "Y" => Ok(Outcome::Draw),
            "Z" => Ok(Outcome::Victory),
            s => Err(s.to_owned()),
        }
    }
}
