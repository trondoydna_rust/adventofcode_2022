use std::env;

fn main() {
    let task = env::args().skip(1).next();

    match task.as_deref() {
        Some("1") => task1(),
        Some("2") => task2(),
        _ => {
            task1();
            task2();
        }
    }
}

fn task1() {
    let output: u32 = common::split_input("\n")
        .iter()
        .map(|line| sum_line(line))
        .sum();

    println!("Task 1: {output}")
}

fn sum_line(line: &str) -> u32 {
    let (first, last) = line.split_at(line.len() / 2);
    let find = first.chars().find(|c| last.contains(|n| c == &n));

    if let Some(c) = find {
        if c.is_ascii_uppercase() {
            u32::from(c) - 38
        } else {
            u32::from(c) - 96
        }
    } else {
        0
    }
}

mod test1 {
    #[test]
    fn example() {
        use crate::sum_line;

        let example = common::split_example("\n");
        assert_eq!(16, sum_line(example.get(0).unwrap()));

        let output: u32 = example.iter().map(|line| sum_line(&line)).sum();
        assert_eq!(157, output);
    }
}

fn task2() {
    let input = common::split_input("\n");
    let mut output = 0;

    let mut input_iter = input.iter();
    for _ in 0..(input.len() / 3) {
        let first = input_iter.next().unwrap();
        let second = input_iter.next().unwrap();
        let third = input_iter.next().unwrap();
        let find = first
            .chars()
            .find(|c| second.contains(|n| c == &n) && third.contains(|n| c == &n));

        output += if let Some(c) = find {
            if c.is_ascii_uppercase() {
                u32::from(c) - 38
            } else {
                u32::from(c) - 96
            }
        } else {
            0
        }
    }

    println!("Task 1: {output}")
}
